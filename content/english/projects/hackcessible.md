---
title: "Hackcessible"
date: 2022-09-023T17:15:00+01:00
author: Grace Faulkner
image: images/blog/hackcessible.jpg
description: "Hackcessible logo"
---

This year, Hackcessible has become a part of Sheffield Bionics!

The Hackcessible website it currently down at the moment but we are working hard to ensure it is recovered soon. Until then, all updates will be shown on this page.

## What is Hackcessible?

Hackcessible is a make-a-thon specifically targeted at disability needs. 

We pair student teams, typically 5-6 students per team, with a challenger. The teams will then meet with their challengers to discuss the need and work together to design a prototype. 

On the 3rd December, World Disability Day, we will have a hackathon to create these prototypes. Not all projects end with a working prototype, but there is the opportunity for students to continue the project afterwards. 

## Who We're Looking For

### Students

Enthusiasm and willingness to learn is the most important quality. 

### Challengers

Do you have a disability that you think could be solved by technology? Get in touch at hackcessible@sheffield.ac.uk!

### Other universities

Do you want to set up Hackcessible on your campus? We would be happy to facillitate! Contact us at hackcessible@sheffield.ac.uk

