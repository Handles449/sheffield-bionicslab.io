---
title: "Arm Team Summer 2021 Updates"
date: 2021-09-24T22:19:27+01:00
author: Xiao Wang
image: images/blog/panic_cat.png
description: "Progress and Aims"
---
## For updates on the Arm project, please click [here](https://www.notion.so/Arm-Project-Home-9c32fd4ecba64f31bb7ef8e75f383ded)!


Following on the work that the previous arm team has started, this summer we spent most of our time on initiating a new design for the hand and arm structure. This design focuses heavily on biomimetic principles trying to replicate the anatomical arm and hand especially its biomechanics and function. The hand is a replica of the CT scan of a real human hand. The design ideology is rooted in using the human anatomy as a blueprint and designing the components such as bones ligaments muscles that will fit together to produce a truly biomimetic limb. 
{{<figure src="/images/blog/1.png" caption="The proto hand" width="75%">}}
{{<figure src="/images/blog/2.png" caption="Human hand anatomy" width="75%">}}
{{<figure src="/images/blog/3.png" caption="The enhanced latest version of the hand" width="75%">}}

By using this method, it results in the exact joints and ranges of movement found in the real hand, coupled with soft 3D printed ligaments, ultimately it will deliver a manufactured hand that behaves exactly like its real counterpart. The forearm region of the design mimics the function of the radius and ulna bones.
{{<figure src="/images/blog/4.png" caption="Human forearm anatomy" width="75%">}}
{{<figure src="/images/blog/5.png" caption="The forearm we’ve printed based on the anatomy" width="75%">}}

The radius rotates around the ulna to rotate the wrist in an action known as pronation and supination. in this model, the ulna has been designed to house the motors responsible for the contraction of the phalanges and therefore is much larger than the natural ulna. due to the larger size of the ulna, the radius also had to be modelled in such a way to still allow it to pass over the ulna while the wrist rotates resulting in the curvature and twists seen in the form of the radius. the elbow joint further mimics the real elbow as the ulna and radius joints are modelled as hinge and ball and socket joints respectively just like the ones in the real human elbow. 
{{<figure src="/images/blog/6.png" caption="CAD for the new arm design" width="75%">}}
{{<figure src="/images/blog/7.png" caption="Linear actuator(linac) - 20% duty cycle, 6v dc, 50mm" width="75%">}}

Even the actuation methods are heavily inspired by the human anatomy, as linacs housed in the forearm act as muscles that contract pulling on nylon wire which acts as the pseudo tendons connected to the 3D printed fingers to contract them. A similar method of actuation is used in the pseudo bicep where servos are used to pull nylon wire connected to the forearm once again mimicking the bicep muscle and tendons respectively. We chose individual actuators that would give the best combination of high torque, high speed, low weight, low size, and low power consumption.
{{<figure src="/images/blog/8.png" caption="Forearm with the linacs inside" width="75%">}}
{{<figure src="/images/blog/9.png" caption="The elbow motor connecting forearm with bicep mash" width="75%">}}                                                          

The problems we need to tackle are:

-find the right battery to drive motors;
-source the most suitable outer skin layer; 
-shrink arm sensor band as the current one will affect the normal daily movement of shoulder and bicep. 

We will be looking for engineers who have keen interests in helping us to solve the above problems and willing to commit oneself to the project, as it could take up quite some spare time. As we have recently applied for HEAR Accreditation, the minimum hours required will be 100 hours in total to spend on this phase. 
