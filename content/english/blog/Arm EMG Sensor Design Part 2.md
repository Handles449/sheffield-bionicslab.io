---
title: "Emg Sensor Design: Part II"
date: 2021-03-18T21:24:28Z
author: By Shunsuke Kawamura
image: /images/blog/emg/title.png
description: "The work Electromyographic (EMG) sub-team has achieved over the autumn semester"
---
This is a continuation of the creation of the EMG sensor. This is part 2 where improvements were made to rectified EMG signal.
For part 1 please look [here](https://sheffield-bionics.gitlab.io/blog/emg-sensor-design/)

## [v2.0] Active Bandpass Filter

We then started to work on the design of a _simple_ active bandpass filter to overcome problems found in [v1]. (More info about bandpass filters: [link](https://www.electronics-tutorials.ws/filter/filter_7.html).)

{{<figure src="/images/blog/emg/part_2/Schematic.png" caption="Electronics Schematic)" width="75%">}}


{{<figure src="/images/blog/emg/part_2/Active.png" caption="Creating Filter Circuits from Home" width="75%">}}


## Noise

After several attempts of data sampling, we frequently observed weird periodic noise interrupting the raw EMG signal measured. I and many people in the society could not figure out what the problem seemed to be. A lot of us were beginners and learning as we went.


{{<youtube 1KpbNx70hPI>}} 


## Solution

{{<youtube 48APmgl6WeQ>}}


As you can see on the right bottom measurement in oscilloscope, the interruption was due to the 50Hz AC signal from USB cable connected to the microcontroller, ESP32.

## [v2.1] Li-ion Battery & Bluetooth connection**

The easiest and simplest way to remove the 50Hz AC noise is isolating the sensor system from USB cable connected to the PC, which needs a wireless communication and a battery. We chose Bluetooth communication in [ESP32](https://doc.riot-os.org/group__boards__esp32__mh-et-live-minikit.html) microcontroller and Li-ion Battery (3.7V/650mAh, x4). We also used [voltage step-down regulator](https://www.ti.com/store/ti/en/p/product/?p=LM340AT-5.0/NOPB) since the microcontroller can only receive 5V. Plus, thanks to the simplified circuit design compared to [v1], we could add a second channel.

{{<youtube DXsGOxCxitQ>}}


## Further Improvement

As you may see above, the signal has noise between around 0-600mV, which is the ground-floating voltage. We will investigate its cause and find a solution. Then, we can order PCB and assemble an actual EMG band soon. The prototype has been designed on Fusion 360 and will be refined. (Sketch and Fusion 360 video below) Additionally, [MPU6050](https://invensense.tdk.com/products/motion-tracking/6-axis/mpu-6050/) gyro and acceleration sensor will be added to the system for the further motion analysis.

## Container Design

{{<figure src="/images/blog/emg/part_2/Design.png" caption="Schematic of EMG Sensor Design" width="75%">}}

{{<youtube pdbo2e2F9Ek>}}
