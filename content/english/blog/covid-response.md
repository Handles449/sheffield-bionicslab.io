---
title: "Bionics: COVID Response"
date: 2020-09-18T23:39:00+01:00
author: Grace Faulkner
image: images/blog/covid/distancing.jpg
description: "The structural team have been working to develop a HALO themed arm for Mark"
---

COVID-19 and the lockdown that followed has had a massive impact on the society. With the Cybathlon the arm team had worked so hard on being postponed indefinitely, and no chance to meet in person to keep working on the project, the future of the projects was uncertain.

However, with weekly virtual meetings for the committee and arm project, Bionics has worked relentlessly over Summer and the progress has been phenomenal, as highlighted in the recent blog posts.

Arm project now has a pilot, Mark, and is working on the design for a personalised arm for him. But in order to complete this, the team needed to be able to meet with Mark and work on the construction of the arm.

{{<figure src="/images/blog/covid/test-fit.jpg" caption="Alex working to check the fitting of the socket" width="90%">}}

To figure out how the team could continue to work and keep safe, a meeting was arranged with Sheffield Students Union to discuss what could be done.

Following this meeting, there are several precautions the team members are taking, including:

 - Wearing face masks and visors when meeting in person
 - Observing social distancing
 - Construction team is reduced to teams of two
 - Regularly using hand sanitizer and disinfectant when meeting in person.

With these precautions, the team have been able to keep working.

But what is the society going to look like going forward?

We are always looking for new members, and this year is no different. Even though the format for meetings this year will be different, we will still need people to join to bring their ideas, skills and knowledge to the teams. Our new Development team is planning a number of workshops to teach useful skills to members.

Meetings are going to still be virtual for the time being. For the arm and leg project, this means that a reduced number of members can be involved in construction, but there are a number of collaborative platforms which will be used for design.

For the BCI, this term they are going to be working on data collection using their headset. However, in order to reduce contact, only the BCI lead will be able to meet with volunteers for this to reduce the group of people who will be meeting.

For new members, the Development team will be meeting virtually. If the iForge opens, there may be an option for some training there, but for the moment the team are assuming that only virtual training will be available.

If you would like to learn more, you can look at our risk assessment [here.](https://drive.google.com/file/d/1ahTXJ9OH47RMuEnoVVwWUu3LTvJ8XHgP/view?usp=sharing)