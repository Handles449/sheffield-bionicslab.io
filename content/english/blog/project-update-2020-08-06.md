---
title: "Project Update"
date: 2020-08-06T10:34:19+01:00
author: Thomas Binu Thomas
image: images/blog/mark_update.jpg
description: "An update on fundraising and the society's focuses"
---

Thank you everyone who has donated so far! We were not able to get enough money to compete for the CYBATHLON in time, but we plan on continuing with our primary focus; building an arm for Mark. Not being part of the competition gives us more time to perfect it. Expect an amazing entry to CYBATHLON 2024!

We have had quite a bit of work done since the last time we had posted. The team were on BBC Radio Sheffield morning talk with Toby Foster. Our hardware team has been working hard designing our hand and have started printing parts. We have tested additional parts to enhance finger pullback. We took pictures and modelled Mark's hand using 3D Zephyr, ready to make the socket. Our firmware team has been working hard using machine learning to take our data with Mark and characterise them for pattern mapping.

{{<figure src="/images/blog/arm_model.jpg" width="100%">}}

We will go into more detail later this week on these different aspects. So stay tuned!
We are also on our last couple of weeks of fundraising and are welcoming any final donations [here](http://gf.me/u/ydvr48).
