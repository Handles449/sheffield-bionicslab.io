---
title: "Emg Sensor Design: Part I"
date: 2021-03-17T21:24:28Z
author: By Shunsuke Kawamura
image: /images/blog/emg/title.png
description: "The work Electromyographic (EMG) sub-team has achieved over the autumn semester"
---

This blog is a summary of the work Electromyographic (EMG) sub-team has achieved over the autumn semester 2020. Even though the work was mostly done by me, Shunsuke, at home due to the pandemic, I fully appreciate the huge support from Kesler, Thomas, Brooks and everyone else from the society.

## Task Overview

As an EMG-sub team member, we began with researching how an EMG sensor band works, to collect electrical signals from a muscle, to develop a prototype to match the level that can be the alternative of preassembled EMG band, [Myo Armband](https://www.570news.com/2016/01/22/thalmic-labs-myo-armband-being-used-to-control-prosthetic-arms/) by Thalmic Labs. We wanted to create our own EMG sensor to easily modify the orientation of the sensor, due to the Myo Armband being built for the forearm while our amputee is an transhumeral (upper-arm) amputee. Our custom EMG sensor is planned to work separately and cohesively with the rest of the hardware and software involved with our Bionic hand. We also could not feasibly source more Myo Armbands as the product is now discontinued, so we wanted to make a cheaper alternative..

{{<figure src="/images/blog/emg/home.png" caption="Work at Home (30,11,2020)" width="75%">}}

{{<figure src="/images/blog/emg/iforge.png" caption="Work at iForge, University of Sheffield (27/11/2020)" width="75%">}}

## [v1] Rectified EMG signal

While we are very motivated to build something, we didn’t know what to do.

Therefore, we decided to begin with reproducing the [work](https://www.instructables.com/Muscle-EMG-Sensor-for-a-Microcontroller/) that someone has done before on Instructables. It sounds crazy and weird, but it was easiest and the most efficient thing we could think of as a complete beginner. This [rectified](https://www.eeeguide.com/precision-full-wave-rectifier/) EMG signal detection system later turned out to be designed around Advancer Technology’s [MyoWare Muscle Sensor](http://www.advancertechnologies.com/p/myoware.html). For these reasons we had a great starting point and a route to improve our design.

[Here is the circuit schematic](https://drive.google.com/file/d/11qiujL1l4sheTSUBy91aFGmCZbWwDh_r/view?usp=sharing)

{{<youtube w4tJHN8TKEA>}}

## [v1] Rectified EMG Signal – Issues

As shown above, the system performed well. However, there were several issues to work on. First, the data is not suitable for signal analysis as the sampling rate was too low and the signal passed through diodes didn’t have negative voltage to be used in high accuracy [motion recognition system](https://sheffield-bionics.gitlab.io/blog/summer-of-ml/) the team completed last summer. Secondly, the circuitry for one channel is too complicated and space-taking (even if a PCB is used) to be applied to multiple-channel system.