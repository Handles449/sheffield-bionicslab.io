---
title: "Test1"
date: 2023-03-02T18:37:26Z
draft: true
author: Tobias McNeil
image: images/blog/cute.jpg
---

# A new post emerges
*For testing purposes only*
## Secondary heading for user retention
**Word to be defined:** Definition of word
## A second secondary heading 
**Another, funky word to be defined:** Even funkier definition
>Quoted by famous person out of context.

{{<figure src="/images/blog/OpAmp.jpg" width="40%">}}
